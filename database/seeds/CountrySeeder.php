<?php

use Illuminate\Database\Seeder;
use GB\Country;
use GB\Image;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fPath = database_path("seeds/src/countries.csv");
        $fHandle = fopen($fPath, "r");
        if ($fHandle !== false) {
            $this->importCountries($fHandle);
        } else {
            throw new \Symfony\Component\Routing\Exception\ResourceNotFoundException("No file accessible at $fPath");
        }
    }

    private function createCountry($countryRow)
    {
        if ($countryRow[0] !== "Name") {
            return Country::firstOrCreate([
                'name' => $countryRow[0],
                'iso_code' => $countryRow[1],
            ]);
        }
        return false;
    }

    private function searchForImage(Country $country)
    {
        $code = strtolower($country->iso_code);
        $pngPath = database_path("seeds/src/flags/$code.png");
        if(file_exists($pngPath)){
            return $pngPath;
        } else {
            return false;
        }
    }

    private function importCountries($fHandle)
    {
        while (($countryRow = fgetcsv($fHandle)) !== false) {
            $country = $this->createCountry($countryRow);
            if($country !== false) {
                $pngPath = $this->searchForImage($country);
                if($pngPath !== false) {
                    $uploadedFile = new \Illuminate\Http\UploadedFile($pngPath, ucwords($country->name).".png", "image/png");
                    $image = Image::fromUploadedFile($uploadedFile);
                    if($image !== null) {
                        $country->setFlagImage($image);
                    }
                }
            }
        }
    }
}