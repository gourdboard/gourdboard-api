<?php

namespace GB;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded = ['id'];

    public function users()
    {
        return $this->hasMany(User::class, 'country_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo(Image::class, "flag_image_id",'id');
    }

    public function setFlagImage(Image $image)
    {
        $this->update([
            'flag_image_id' => $image->id,
        ]);
    }
}
