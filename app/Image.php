<?php

namespace GB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class Image extends Model
{
    protected $guarded = ['id'];

    public static function fromUploadedFile(UploadedFile $file)
    {
        if (self::isAcceptableMimeType($file->getMimeType())) {
            $s3Path =  self::toS3($file);
            list($w, $h, $type, $atrr) = getimagesize($file);
            return Image::firstOrCreate([
                'path' => $s3Path,
                'w' => $w,
                'h' => $h,
            ]);
        }
    }

    public static function isAcceptableMimeType($type) : bool
    {
        $acceptableTypes = [
            'image/png',
            'image/gif',
            'image/jpeg'
        ];
        return in_array($type, $acceptableTypes);
    }

    public static function toS3(UploadedFile $file)
    {
        $client =  self::createS3Client();
        $key = uniqid().$file->getClientOriginalName();
        $result = $client->putObject([
            'Bucket' => env("AWS_BUCKET"),
            'Key' => $key,
            'Body' => file_get_contents($file),
        ]);
        return $key;
    }

    private static function createS3Client()
    {
        return new S3Client([
            'credentials' => [
                'key' => env("AWS_ACCESS_KEY_ID"),
                'secret' => env("AWS_SECRET_ACCESS_KEY")
            ],
            "version" => "latest",
            "region" => env("AWS_DEFAULT_REGION"),
        ]);
    }

    public function getSignedURL()
    {
        $client = self::createS3Client();
        $cmd = $client->getCommand("GetObject",[
            "Bucket" => env("AWS_BUCKET"),
            "Key" => $this->path,
        ]);
        return (string) $client->createPresignedRequest($cmd, "+20 minutes")->getUri();
    }
}
