<?php

namespace GB\Http\Requests\Auth;

use GB\Rules\IdentifierExists;
use Illuminate\Foundation\Http\FormRequest;

class AuthenticateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'identifier' => ['required', new IdentifierExists ],
            'password' => 'required'
        ];
    }
}
