<?php

namespace GB\Http\Controllers;

use GB\Http\Requests\Auth\AuthenticateRequest;
use GB\Http\Requests\Auth\RegisterRequest;
use GB\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(RegisterRequest $registerRequest)
    {
        $user = User::fromRegister($registerRequest);

        return response()->json($user, 200);
    }

    public function authenticate(AuthenticateRequest $authenticateRequest)
    {
        $email = User::emailFromIdentifier($authenticateRequest->get('identifier'));
        $pass =  $authenticateRequest->get('password');
        if (Auth::attempt(['email' => $email, 'password' => $pass])) {
            return response()->json(['success' => true, 'api_token' => Auth::user()->api_token]);
        } else {
            return response()->json(['success' => false]);
        }
    }
}
